"""importing csv file"""
import csv
import matplotlib.pyplot as plt
import pandas as pd

def calc():
    """ # collecting the .csv files and converting into data"""
    filepath = "/home/akhil118/Desktop/gitlab/ipl/data/matches.csv"
    with open(filepath, "r", encoding='UTF-8') as file:
        csv_reader = csv.DictReader(file)
        matchsdata = list(csv_reader)

    years = []
    teams = []
    for season in matchsdata:
        years.append(season["season"])
        teams.append(season["team1"])
    years = sorted(list(set(years)))
    teams = sorted(list(set(teams)))

    yearsdata = {}

    for year in years:
        teamsdata = {}
        for team in teams:
            teamsdata[team] = 0

        yearsdata[year] = teamsdata

    for match in matchsdata:
        try :
            year = match["season"]
            team = match["winner"]

            yearsdata[year][team] += 1
        except KeyError :
            pass

    # print(yearsdata)
    return yearsdata, teams


def plot(yearsdata ,teams):
    """docstring"""
    chartlist = []
    seasons = list(yearsdata.keys())
    count = 0
    seasonsdata = list(yearsdata.values())
    for eachitem in seasonsdata:

        valuesofchart = [seasons[count]]+list(eachitem.values())
        chartlist.append(valuesofchart)
        count=count+1

    stackedchart = pd.DataFrame(chartlist,columns=["Teams"]+teams)

    stackedchart.plot(x='Teams', kind='bar', stacked=True,
                    title='Stacked Bar Graph of matches won per teams per season')
    plt.show()


def main():
    """docstring"""
    wondata, teams = calc()
    plot(wondata, teams)


if __name__ == "__main__":
    main()
    