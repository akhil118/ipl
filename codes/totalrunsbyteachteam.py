"""importing csv file"""
import csv
import matplotlib.pyplot as plt


def calc():
    """ collecting the .csv files and converting into data"""

    filepath= "/home/akhil118/Desktop/gitlab/ipl/data/deliveries.csv"
    with open(filepath, "r", encoding='UTF-8') as file:
        csv_reader = csv.DictReader(file)
        matchdata = list(csv_reader)

    teams_dict = {}
    for team in matchdata:
        batting_team = team["batting_team"]
        if batting_team in teams_dict:
            teams_dict[batting_team] = teams_dict[batting_team]+int(team["total_runs"])
        else :
            teams_dict[batting_team] = int(team["total_runs"])

    return teams_dict


def plot(teamsdict):
    """docstring"""
    plt.barh(list(teamsdict.keys()), list(teamsdict.values()))
    plt.show()


def main():
    """docstring"""
    teamswithruns = calc()
    plot(teamswithruns)


if __name__ == "__main__":
    main()
