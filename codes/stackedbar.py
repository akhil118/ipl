"""importing csv file"""
import csv
import matplotlib.pyplot as plt
import pandas as pd

def calc():
    """ # collecting the .csv files and converting into data"""
    filepath="/home/akhil118/Desktop/gitlab/ipl/data/matches.csv"
    with open(filepath, "r", encoding='UTF-8') as file:
        csv_reader = csv.DictReader(file)
        matchsdata = list(csv_reader)
    # collecting data from "matches.csv" file and
    # finding StackedchartOfMatchesPlayedByTeamAndBySeason

    years = []
    teams = []
    for season in matchsdata:
        years.append(season["season"])
        teams.append(season["team1"])
    years = sorted(list(set(years)))
    teams = sorted(list(set(teams)))

    yearsdata = {}

    for year in years:
        teamsdata = {}
        for team in teams:
            teamsdata[team] = 0
        yearsdata[year] = teamsdata

    for match in matchsdata:

        year = match["season"]
        team1 = match["team1"]
        team2 = match["team2"]

        yearsdata[year][team1] += 1
        yearsdata[year][team2] += 1

    # print(yearsdata)
    return yearsdata,teams

def plot(yearsdata, teams):
    """docstring"""
    chartlist = []
    seasons = list(yearsdata.keys())
    count = 0
    seasonsdata = list(yearsdata.values())
    for eachitem in seasonsdata:

        valuesofchart = [seasons[count]]+list(eachitem.values())
        chartlist.append(valuesofchart)
        count=count+1

    stackedchart = pd.DataFrame(chartlist,columns=["Teams"]+teams)

    stackedchart.plot(x='Teams', kind='bar', stacked=True,
                    title='Stacked Bar Graph of matches per teams per season')
    plt.show()


def main():
    """docstring"""
    yearsdata, teams = calc()
    plot(yearsdata, teams)

if __name__ == "__main__":
    main()
