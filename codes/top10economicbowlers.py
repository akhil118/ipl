"""importing csv file"""
import csv
import matplotlib.pyplot as plt

def rawdata(filename):
    """ # collecting the .csv files and converting into data"""
    with open(filename, "r", encoding='UTF-8') as file:
        csv_reader = csv.DictReader(file)
        csvdata = list(csv_reader)
    return csvdata


MATCHESPATH = "/home/akhil118/Desktop/gitlab/ipl/data/matches.csv"
DELIVERIESPATH = "/home/akhil118/Desktop/gitlab/ipl/data/deliveries.csv"

def main(filem,filed):
    """doc string"""
    # collecting data from "matches.csv" file and
    # collecting data from "matches.csv" and "deliveries.csv" files and
    # finding top economical bowler in 2015

    # importing the file of deliveries.csv and converting them into data
    deliveriesdata = rawdata(filed)
    matchdata = rawdata(filem)

    matchid = []
    for ids in matchdata:

        if ids["season"] == "2015":
            matchid.append(ids["id"])

    bowlers = []

    for data in deliveriesdata:

        if data["match_id"] in matchid:
            bowlers.append(data["bowler"])

    bowlers = list(set(bowlers))

    values = [0]*len(bowlers)

    bowlers_with_runs = dict(zip(bowlers, values))
    bowlers_with_overs = dict(zip(bowlers, values))

    for data in deliveriesdata:

        if data["match_id"] in matchid:
            bowlers_with_runs[data["bowler"]] += int(data["total_runs"])
            bowlers_with_overs[data["bowler"]] += 1

    # print(bowlersWithOvers,bowlersWithRuns)
    # print(len(bowlersWithOvers.keys()),len(bowlersWithRuns.keys()))

    total_bowlers = list(bowlers_with_runs.keys())

    total_runs_by_bowler = list(bowlers_with_runs.values())
    total_overs_by_bowler = list(bowlers_with_overs.values())

    bowler_economy_list = []
    count = 0
    for bowler in total_bowlers:

        economy = (total_runs_by_bowler[count]/total_overs_by_bowler[count])*6
        bowler_details = [economy, bowler]
        bowler_economy_list.append(bowler_details)
        count = count+1

    top10_economy_bowlers_list = sorted(bowler_economy_list)[:10]

    top10_economy_bowlers = {}

    for bowlers in top10_economy_bowlers_list:

        top10_economy_bowlers[bowlers[-1]] = bowlers[0]


    plt.barh(list(top10_economy_bowlers.keys()),list(top10_economy_bowlers.values()))
    plt.show()
    return top10_economy_bowlers



if __name__ == "__main__":

    main(MATCHESPATH, DELIVERIESPATH)
