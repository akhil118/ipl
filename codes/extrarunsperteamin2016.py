"""importing csv file"""
import csv
import matplotlib.pyplot as plt

def rawdata(filename):
    """ # collecting the .csv files and converting into data"""
    with open(filename, "r", encoding='UTF-8') as file:
        csv_reader = csv.DictReader(file)
        data = list(csv_reader)
    return data


def calc():
    """# collecting data from "matches.csv" file and
    # collecting data from "matches.csv" and "deliveries.csv" files and
    # finding the extra runs conceded per team in 2016"""
    matches_path = "/home/akhil118/Desktop/gitlab/ipl/data/matches.csv"
    deliveries_path = "/home/akhil118/Desktop/gitlab/ipl/data/deliveries.csv"
    # importing the file of deliveries.csv and converting them into data
    deliveriesdata = rawdata(deliveries_path)
    matchdata = rawdata(matches_path)
    teams = []
    for data in deliveriesdata:
        teams.append(data["batting_team"])

    teams = list(set(teams))
    values = [0]*len(teams)
    teams_with_runs = dict(zip(teams, values))

    matchid = []
    for ids in matchdata:

        if ids["season"] == "2016":
            matchid.append(ids["id"])

    # print(id)

    for data in deliveriesdata:

        if data["match_id"] in matchid:
            teams_with_runs[data["batting_team"]] += int(data["extra_runs"])

    return teams_with_runs


def plot(teams_with_runs):
    """docstring"""
    plt.barh(list(teams_with_runs.keys()), list(teams_with_runs.values()))
    plt.show()


def main():
    """docstring"""
    teams_with_runs = calc()
    plot(teams_with_runs)


if __name__ == "__main__":
    main()
