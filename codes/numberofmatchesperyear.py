"""importing csv file"""
import csv
import matplotlib.pyplot as plt
# import matplotlib.pyplot as plt

def calc():
    """ # collecting the .csv files and converting into data"""
    filename = "/home/akhil118/Desktop/gitlab/ipl/data/matches.csv"
    with open(filename, "r", encoding='UTF-8') as file:
        csv_reader = csv.DictReader(file)
        matchdata = list(csv_reader)

    years = []
    for year in matchdata:
        years.append(year["season"])

    years = sorted(list(set(years)))

    # print(years)
    matches = [0]*len(years)
    data = dict(zip(years, matches))
    # print(data)

    for matches in matchdata:
        data[matches["season"]] = data[matches["season"]] + 1

    # print(data)
    return data


def plot(data):
    """docstring"""
    plt.barh(list(data.keys()), list(data.values()))
    plt.show()


def main():
    """docstring"""
    
    numberofmatchesperyear = calc()
    plot(numberofmatchesperyear)


if __name__ == "__main__":

    main()
