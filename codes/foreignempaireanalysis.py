"""importing csv file"""
import csv
import matplotlib.pyplot as plt


def calc():
    """ # collecting the .csv files and converting into data"""
    filepath = "/home/akhil118/Desktop/gitlab/ipl/data/umpires.csv"
    with open(filepath, "r",encoding="UTF-8") as file:
        csv_reader = csv.DictReader(file)
        umpiresdata= list(csv_reader)

    # collecting the data from "umpires.csv" and
    # getting barchart of umpires on basis of country and no of umpires

    # importing the file of umpires.csv and converting them into data

    countries = []

    for data in umpiresdata:

        countries.append(data["Nationality"].strip())

    unique_countries = list(set(countries))

    unique_countries.remove("India")
    # uniqueCountries.remove("Nationality")
    # print(uniqueCountries)

    nums = [0]*len(unique_countries)

    countriesdata = dict(zip(unique_countries, nums))

    for data in unique_countries:
        
        countriesdata[data] = countries.count(data)

    # print(countriesdata)
    return countriesdata


def plot(countriesdata):
    """docstring"""
    plt.barh(list(countriesdata.keys()), list(countriesdata.values()))
    plt.show()



def main():
    """docstring"""
    foreign_umpires = calc()
    plot(foreign_umpires)


if __name__ == "__main__":
    main()
    