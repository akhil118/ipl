"""importing csv file"""
import csv
import matplotlib.pyplot as plt


def calc():
    """ collecting the .csv files and converting into data"""
    filepath = "/home/akhil118/Desktop/gitlab/ipl/data/deliveries.csv"
    with open(filepath, "r", encoding='UTF-8') as file:
        csv_reader = csv.DictReader(file)
        matchdata = list(csv_reader)
    # collecting data from "deliveries.csv" and
    # finding the playerscored topruns for RCB in entire game

    playerdata = {}
    for runs in matchdata:
        if runs["batting_team"] == "Royal Challengers Bangalore":
            batsman = runs["batsman"]

            if batsman in playerdata:
                playerdata[batsman] = playerdata[batsman]+int(runs["total_runs"])
            else :
                playerdata[batsman] = int(runs["total_runs"])

    top_10_rcb_batsman = dict(sorted(playerdata.items(),key = lambda x : x[1])[-10:])

    return top_10_rcb_batsman


def plot(top_10_rcb_batsman):
    """docstring"""
    plt.barh(list(top_10_rcb_batsman.keys()), list(top_10_rcb_batsman.values()))
    plt.show()

def main():
    """docstring"""
    top_10_rcb_batsman = calc()
    plot(top_10_rcb_batsman)


if __name__ == "__main__":
    main()
    