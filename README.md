
## Name
IPL data project.

## Description

* we will importrequired modules like matplotlib.pyplot and csv 
* now we have to collect raw data and store them in a file
* with the help of those files and modules we can create this code as following steps

1. collecting the .csv files and converting into data
2. collecting the data from "deliveries.csv" and getting bar chart for total runs of each team 
3. collecting data from "deliveries.csv" and getting bar chart for the player scored topruns for RCB in entire game
4. collecting the data from "umpires.csv" and finding the barchart of umpires on basis of country and no of umpires per country
5. collecting data from "matches.csv" file and finding Stacked chart of matches played by team by season
6. collecting data from "matches.csv" file and getting bar chart for Number of matches played per year for all the years in IPL
7. collecting data from "matches.csv" file and getting bar chart for Number of matches won per team per year in IPL
8. Collecting data from "matches.csv" and "deliveries.csv" file and getting bar chart for Extra runs conceded per team in the year 2016
9. Collecting data from "matches.csv" file and getting bar chart for Top 10 economical bowlers in the year 2015


## Project status
Completed according to the requirement updates and modifications according to post instruction have to be done. 
